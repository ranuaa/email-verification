﻿using MailKit.Security;
using MimeKit.Text;
using MimeKit;
using MailKit.Net.Smtp;
using System.Text;

namespace taskMail.Services.EmailService
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _configuration;
        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public void sendEmail(DailyNotes request)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(_configuration.GetSection("EmailUserName").Value));
            email.To.Add(MailboxAddress.Parse(request.emailTo));
            email.Subject = "Daily Notes";
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(@"<h1 style=""text-align: center"" >Daily notes</h1> </br>");
            foreach (string note in request.notes)
            {
                sb.AppendFormat($@"<li style=""margin-left: 25%"">{note}</li>");
            }
            email.Body = new TextPart(TextFormat.Html) { Text = sb.ToString()};
            using var smtp = new SmtpClient();
            smtp.Connect(_configuration.GetSection("EmailHost").Value, 587, SecureSocketOptions.StartTls);
            smtp.Authenticate(_configuration.GetSection("EmailUserName").Value, _configuration.GetSection("EmaiPassword").Value);
            smtp.Send(email);
            smtp.Disconnect(true);
        }
    }
}
